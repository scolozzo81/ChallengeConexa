package com.conexa_challenge.utils

import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.ui.groupies_viewholders.ProductItem
import com.conexa_challenge.ui.interfaces.OnProductClick


fun ArrayList<Product>.setDateProducts(): List<Product> {
    this.forEach {
        it.createdAt = System.currentTimeMillis()
    }
    return this
}

fun List<Product>.replace(newValue: Product, block: (Product) -> Boolean): List<Product> {
    return map {
        if (block(it)) newValue else it
    }
}
fun List<Product>.toProductTem(listener:OnProductClick) : List<ProductItem>{
    return this.map {
        ProductItem(it,listener)
    }
}
fun List<ProductItem>.toProduc() : List<Product>{
    return this.map {
        Product(it.product.id,it.product.title,it.product.price,it.product.description,it.product.category,it.product.image,it.product.rating,it.product.quantity,it.product.createdAt)
    }
}