package com.conexa_challenge.repository.services

import com.conexa_challenge.repository.services.interfaces.APIService
import com.conexa_challenge.repository.services.interfaces.NetworkService
import com.conexa_challenge.db.entities.Product
import javax.inject.Inject

open class Service @Inject constructor(private val service: APIService): NetworkService {

    override suspend fun getAllProducts() : List<Product>{
       return  service.getAllProducts()
    }

    override suspend fun getAllCategories() : List<String>{
      return  service.getAllCategories()
    }
}