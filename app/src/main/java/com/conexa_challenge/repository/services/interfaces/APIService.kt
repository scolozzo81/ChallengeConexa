package com.conexa_challenge.repository.services.interfaces

import com.conexa_challenge.db.entities.Product
import retrofit2.http.GET

interface APIService {
    companion object {
        private const val PRODUC_RELATIVE_URL = "products"
        private const val CATEGORY_RELATIVE_URL = "products/categories"

    }

    @GET("$PRODUC_RELATIVE_URL")
    suspend fun getAllProducts() :List<Product>


    @GET("$CATEGORY_RELATIVE_URL")
    suspend fun getAllCategories() :List<String>
}