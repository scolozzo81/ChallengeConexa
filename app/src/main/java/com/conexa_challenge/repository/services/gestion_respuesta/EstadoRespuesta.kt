

/**
 * Enum about status
 * of Contenedor
 */
enum class EstadoRespuesta {
    SUCCESS,
    ERROR,
    LOADING
}