package com.conexa_challenge.repository.services.interfaces

import com.conexa_challenge.db.entities.Product

interface Repository {
    suspend fun getAllProducts() : List<Product>
    suspend fun getAllCategories() : List<String>


}