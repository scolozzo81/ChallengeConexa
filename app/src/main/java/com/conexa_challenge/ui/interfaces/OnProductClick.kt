package com.conexa_challenge.ui.interfaces

import com.conexa_challenge.db.entities.Product

interface OnProductClick {
    fun onPlus(product: Product)
    fun onSubstrac(product: Product)
}