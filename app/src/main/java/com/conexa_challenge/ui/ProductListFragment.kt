package com.conexa_challenge.ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conexa_challenge.R
import com.conexa_challenge.databinding.FragmentListBinding
import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.ui.groupies_viewholders.ProductItem
import com.conexa_challenge.ui.interfaces.OnProductClick
import com.conexa_challenge.utils.toProductTem


import com.conexa_challenge.viewModels.ProductViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ProductListFragment : Fragment(R.layout.fragment_list), OnProductClick {
    private val productViewModel: ProductViewModel by activityViewModels()
    private var products = ArrayList<Product>()
    private var productItems = ArrayList<ProductItem>()
    private lateinit var layoutM: RecyclerView.LayoutManager
    private lateinit var binding: FragmentListBinding
    private var isLoading = false
    private lateinit var listener : OnProductClick
    private  var productAdapter= GroupAdapter<ViewHolder>()
    private lateinit var adatador: ArrayAdapter<CharSequence>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        binding = FragmentListBinding.inflate(layoutInflater)
        setLayout()
        setListeners()
        isLoading = true
        setObservers()
        if (productViewModel.products.value == null) productViewModel.loadProducts()
        productViewModel.getCategories()
        return binding.root
    }

    private fun setObservers() {
          listener =this
        productViewModel.products.observe(this.viewLifecycleOwner, Observer {
            productItems.addAll((it.toProductTem(listener)))
            productAdapter.addAll(productItems)
            updateRecyclerview()
        })
        productViewModel.categories.observe(this.viewLifecycleOwner, Observer {
            var categoryList = ArrayList<String>()
            categoryList.add(getString(R.string.allcategories))
            categoryList.addAll(it)
            adatador = context?.let { c ->
                ArrayAdapter(
                    c, android.R.layout.simple_spinner_item,
                    categoryList as List<CharSequence>
                )
            }!!
            binding.spinner.adapter = adatador

        })
    }

    private fun setListeners() {
        binding.ivListadoCarrito.setOnClickListener {
            findNavController().navigate(R.id.action_listadoFragment_to_carritoFragment)
        }
        binding.svProducts.setOnQueryTextListener(object : SearchView.OnQueryTextListener, android.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                context?.let {
                    closeSoftKeyboard(it, binding.svProducts)
                }
                return updateProductListSearchView(query)
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return updateProductListSearchView(newText)
            }
        })
        binding.ivSortAsc.setOnClickListener {
            var l =  productItems.sortedBy { it.product.title }
            productItems.clear()
            productItems.addAll(l)
            productAdapter.clear()
            productAdapter.addAll(productItems)
            updateRecyclerview()
        }
        binding.ivSortDesc.setOnClickListener {
            var l = productItems.sortedBy { it.product.title }.reversed()
            productItems.clear()
            productItems.addAll(l)
            productAdapter.clear()
            productAdapter.addAll(productItems)
            updateRecyclerview()
        }
        binding.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                var l = ( productViewModel.getProductByCategory(
                    binding.spinner.selectedItem.toString(),
                    position == 0
                ) as ArrayList<Product>).toProductTem(listener)
                productItems.clear()
                productItems.addAll(l)
                productAdapter.clear()
                productAdapter.addAll(productItems)
                updateRecyclerview()
            }

        }
    }

    private fun updateProductListSearchView(query: String): Boolean {
        if (query.length > 0) {
              productAdapter.clear()
              val list = productViewModel.getProductFiltered(query)
            if (!list.isNullOrEmpty()) {
                productItems.clear()
                productItems.addAll(list.toProductTem(listener))
                productAdapter.clear()
                productAdapter.addAll(productItems)
            }else
                if (query.length in 3..4)
                    Toast.makeText(context, getString(R.string.without_result), Toast.LENGTH_LONG)
                        .show()
            updateRecyclerview()
            return true
        } else {
            reloadList()
            return true
        }
    }

    private fun reloadList() {
        isLoading = true
        binding.progressBar.visibility = View.VISIBLE
        productAdapter.clear()
        productViewModel.getProducts()?.let {
            productItems.clear()
            productItems.addAll(it.toProductTem(this))
            productAdapter.addAll(productItems) }
        updateRecyclerview()
    }

    private fun updateRecyclerview() {
        if (!products.isNullOrEmpty()) {
            if (binding.ProductosRecycler.adapter == null) {
                productAdapter.addAll(products.toProductTem(this))
                binding.ProductosRecycler.adapter = productAdapter
            } else
                binding.ProductosRecycler.adapter?.notifyDataSetChanged()

        } else binding.ProductosRecycler.adapter?.notifyDataSetChanged()

        isLoading = false
        binding.progressBar.visibility = View.GONE
    }

    private fun setLayout() {
        binding.progressBar.visibility = View.GONE
        binding.ProductosRecycler.apply {
            layoutM = LinearLayoutManager(binding.root.context)
            this.layoutManager = layoutM
            this.adapter= productAdapter
        }

    }

    override fun onPlus(product: Product) {
        productViewModel.plus(product)
    }

    override fun onSubstrac(product: Product) {
        productViewModel.substrac(product)
    }

    private fun closeSoftKeyboard(context: Context, v: View) {
        val iMm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        iMm.hideSoftInputFromWindow(v.windowToken, 0)
        v.clearFocus()
    }
}