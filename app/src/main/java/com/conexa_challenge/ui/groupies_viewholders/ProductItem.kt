package com.conexa_challenge.ui.groupies_viewholders

import com.conexa_challenge.ui.interfaces.OnProductClick
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.conexa_challenge.R
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.item_product.view.*
import com.conexa_challenge.db.entities.Product

class ProductItem(val product: Product,val listener : OnProductClick) : Item<ViewHolder>() {

    override fun getLayout() = R.layout.item_product

    override fun bind(viewHolder: ViewHolder, position: Int) {
        var context= viewHolder.itemView.context
        viewHolder.itemView.tvNombreProducto.text = product.title
        viewHolder.itemView.tvPrecioProducto.text = product.price.toString()
        viewHolder.itemView.tvCantidadProducto.text = product.quantity.toString()

        Glide.with(context)
            .load(product.image)
            .placeholder(R.drawable.cart_plus)
            .error(R.drawable.cart_remove)
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .centerInside()
            .into(viewHolder.itemView.imgItemProducto)

           viewHolder.itemView.iv_subtract.setOnClickListener{
                if(product.quantity>0) {
                    product.quantity--
                    viewHolder.itemView.tvCantidadProducto.text = product.quantity.toString()
                    listener.onSubstrac(product)
                }
            }

          viewHolder.itemView.iv_plus.setOnClickListener{
            if(product.quantity<99) {
                product.quantity++
                viewHolder.itemView.tvCantidadProducto.text = product.quantity.toString()
                listener.onPlus(product)
            }
        }

    }
}