package com.conexa_challenge.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.Fragment
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.conexa_challenge.R
import com.conexa_challenge.databinding.FragmentShopBinding
import com.conexa_challenge.db.entities.Product
import com.conexa_challenge.ui.groupies_viewholders.ProductItem
import com.conexa_challenge.ui.interfaces.OnProductClick
import com.conexa_challenge.utils.toProduc
import com.conexa_challenge.utils.toProductTem
import com.conexa_challenge.viewModels.ProductViewModel
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ShopFragment : Fragment(R.layout.fragment_shop), OnProductClick {
    private val productViewModel: ProductViewModel by activityViewModels()
    private lateinit var binding: FragmentShopBinding
    private var productList= ArrayList<ProductItem>()
    // private  var productList: List<Product>? = null
    private lateinit var layoutM: RecyclerView.LayoutManager
    //   private lateinit var productAdapter: ProductAdapter
    private var productAdapter = GroupAdapter<ViewHolder>()
    private lateinit var listener : OnProductClick

    override fun onCreateView(    inflater: LayoutInflater,        container: ViewGroup?,        savedInstanceState: Bundle?    ): View? {
        binding = FragmentShopBinding.inflate(layoutInflater)
        initLayout()
        setListeners()
        setObservers()
        productViewModel.getSelectedproducts()?.toProductTem(this)?.let {
            productList.addAll(it)
            productAdapter.addAll(productList)
            binding.ProductosRecycler.adapter?.notifyDataSetChanged()
        }
        productViewModel.computingOverview()
        return binding.root
    }

    private fun setObservers() {
        listener= this
        productViewModel.overview.observe(this.viewLifecycleOwner, Observer {
            binding.tvTotal.text =
                "$ " + it.first.toString().split(".")[0] + "." + it.first.toString()
                    .split(".")[1].take(2)
            binding.tvQuantity.text = it.second.toString()
        })
    }

    private fun initLayout() {
        layoutM = LinearLayoutManager(binding.root.context)
        binding.ProductosRecycler.layoutManager = layoutM
        binding.ProductosRecycler.apply {
            layoutM = LinearLayoutManager(binding.root.context)
            this.layoutManager = layoutM
            this.adapter= productAdapter
        }
    }


    private fun setListeners() {
        binding.ivBackListado.setOnClickListener {
            findNavController().navigate(R.id.action_carritoFragment_to_listadoFragment)
        }
        binding.btnClearShop.setOnClickListener {
            productList.clear()
            productAdapter.clear()
            productViewModel.clearShop()
            binding.tvQuantity.text= getString(R.string.zero)
            binding.tvTotal.text= getString(R.string.zero_total)
            Toast.makeText(context, getString(R.string.card_shop_cleared), Toast.LENGTH_SHORT)
                .show()
            findNavController().navigate(R.id.action_carritoFragment_to_listadoFragment)
        }
    }

    override fun onPlus(product: Product) {
        productViewModel.plus(product)
    }

    override fun onSubstrac(product: Product) {
        if (product.quantity == 0) {
            val l= productViewModel.delete(product.id, productList.toProduc())?.toProductTem(listener)
            productList.clear()
            if (l != null) {
                productList.addAll(l)
            }
            productAdapter.clear()
            productAdapter.addAll(productList)
            binding.ProductosRecycler.adapter?.notifyDataSetChanged()
            Toast.makeText(
                context, getString(R.string.product_deleted), Toast.LENGTH_SHORT
            ).show()
        }
        productViewModel.substrac(product)
    }
}