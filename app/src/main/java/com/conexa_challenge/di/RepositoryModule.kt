package com.conexa_challenge.di

import com.conexa_challenge.repository.services.interfaces.Repository
import com.conexa_challenge.repository.RepositoryImp
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun getRepository( implRepository: RepositoryImp
    ): Repository
}


