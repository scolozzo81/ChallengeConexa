package com.conexa_challenge.db.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import com.conexa_challenge.repository.services.pojos.principales.Rating
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id"])
data class Product(
    @SerializedName("id") val id : Int,
    @SerializedName("title") val title : String,
    @SerializedName("price") val price : Double,
    @SerializedName("description") val description : String,
    @SerializedName("category") val category : String,
    @SerializedName("image") val image : String,
    @SerializedName("rating") val rating : Rating,
    @ColumnInfo(name="quantity")
    var quantity:Int = 0,
    @ColumnInfo(name="Created_at")
    var createdAt:Long = 0
)
