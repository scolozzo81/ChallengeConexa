package com.conexa_challenge.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.conexa_challenge.db.entities.Product
import java.util.ArrayList

@Dao
interface ProductDAO {

   @Query("SELECT * FROM Product")
    suspend fun getAllProducts(): List<Product>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProduct(products: List<Product>)

    @Query("SELECT COUNT(*) FROM Product")
    suspend fun getAmountProducts(): Long

     @Query("SELECT Product.Created_at FROM Product Order by Created_at DESC Limit 1")
    suspend fun getLastDateProducts(): Long
}